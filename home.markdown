CIRCLE install

Salt minion telepítése: 

```
sudo add-apt-repository ppa:saltstack/salt; 
sudo apt-get update; 
sudo apt-get install salt-minion;
echo "master: 10.0.0.26" | sudo tee /etc/salt/minion.d/master.conf;
sudo restart salt-minion
```

Salt master
```
sudo restart salt-minion
sudo salt-key -A <minion-name>
```

Using the CIRCLE salt repository
```
sudo mkdir -p /srv
sudo chown cloud:cloud /srv
git clone
sudo salt 'cloud-1115' saltutil.refresh_pillar
sudo salt 'cloud-1115' state.sls allinone
```